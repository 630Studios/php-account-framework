<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Account Framework: New Install</title>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,900' rel='stylesheet' type='text/css'>
<link href="css/base.css" rel="stylesheet" type="text/css" />
</head>
<body>
        <div class="credits">
    	<div  class="title">SETUP</div>
        <div class="repo"><blockquote>Thank you for downloading my PHP Account Framework. All you have to do now is <a href="includes/accounts/setup/setup.php">Setup</a> the account system.</blockquote></div>
        
    </div> 
    <?php
	generateBitBucketProfile("Author Information", "630Studios", "StrikeBackStudios@gmail.com");


?>
    </div>
</body>
</html>


<?php
function generateBitBucketProfile($title, $accountName, $email)
{
	$data = file_get_contents("https://api.bitbucket.org/1.0/users/" . $accountName);
	$jSonData = json_decode(file_get_contents("https://api.bitbucket.org/1.0/users/" . $accountName));
	
	?>
    <div class="credits">
    	<div  class="title"><?=$title;?></div>
   	    <img class="bitbucketAvatar" src="https://bitbucket.org/account/<?=$accountName;?>/avatar/64/" />
        <div>Created By: <?=$jSonData->user->first_name;?> <?=$jSonData->user->last_name;?></div>
		<div>Email: <?=$email;?></div>
		<div>Bitbucket: <a href="http://bitbucket.org/<?=$accountName;?>/" target="_blank">http://bitbucket.org/<?=$accountName;?>/</a></div>
    	<div class="title">Repositories</div>
	<?php

	foreach ($jSonData->repositories as $repo)
	{
		?>
		<div class="repo"><a href="http://bitbucket.org/<?=$accountName;?>/<?=$repo->slug;?>/"  target="_blank"><?=$repo->name;?></a><br /><?=$repo->description?></div>
        <?php
	}
}

?>
