<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function checkForVerification()
{
	$db = $GLOBALS["db"];
	
	if (isset($_POST["verification"]))
		$activation = mysqli_real_escape_string($db,$_POST["verification"]);
	
	if (isset($_GET["verification"]))
		$activation = mysqli_real_escape_string($db,$_GET["verification"]);
		
	if (!isset($activation))
		return false;

	if (isset($_POST["username"]))
		$username = mysqli_real_escape_string($db,$_POST["username"]);
	
	if (isset($_GET["username"]))
		$username = mysqli_real_escape_string($db,$_GET["username"]);
		
	if (!isset($username))
		return false;

		$sqlStr = "SELECT * FROM users WHERE username = '{$username}' AND verificationCode = '{$activation}'";

		$results = $db->query($sqlStr);
		$resultCount = $results->num_rows;
		
		if ($resultCount < 1)
		{
			invalidVerificationResponse("Invalid username or verification code.");
			return false;
		}else{
			$sqlStr = "UPDATE users SET verificationCode='0' WHERE username = '{$username}' AND verificationCode = '{$activation}'";
			$results = $db->query($sqlStr);
			validVerificationResponse(file_get_contents(ACCOUNT_TEMPLATE_DIR . "/responses/veri_thankyou.html"));
			return true;
		}

}

function validVerificationResponse($message, $redirectPage)
{
?>
<div id="response">
	<div id="title">Verification Success</div>
	<div id="outcome" class="hidden">1</div>
   	<div id="message" class="message"><?php echo($message);?></div>	
   	<div id="redirect" class="hidden" ><?php echo($redirectPage);?></div>
</div>
<?php
}

function invalidVerificationResponse($message)
{
?>
<div id="response">
	<div id="title">Verification Faliure</div>
	<div id="outcome" class="hidden">0</div>
   	<div id="reason" class="message"><?php echo($message);?></div>	
</div>
<?php
}
?>