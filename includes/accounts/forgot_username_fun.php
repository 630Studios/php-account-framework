<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function handleForgotUsername()
{
		
		if(!isset($_POST['email']))
			return false;
			

		$db = $GLOBALS["db"];

		
		$email = trim(mysqli_real_escape_string($db,$_POST["email"]));
		
		if (strlen($email) < 5)
		{
			failResponse("Email is not in valid format.");
			return false;	
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			failResponse("Email is not in valid format.");
			return false;
			//invalid email format
		}
				
		$results = $db->query("SELECT * FROM users WHERE email='" . $email . "'");
		$userCount = $results->num_rows;
		
		if ($userCount <= 0) // if we have more then one record either the username or email address is already in use.
		{
			failResponse("No such email in use.");
			return false;
//			invalidLoginResponse("Username or Email address already in use.");
		}else{ // no matches, we can add the user
			$row = $results->fetch_assoc();
            $username = $row["username"];
			sendForgotUsername($email, ACCOUNT_EMAIL, $username);
			successResponse(file_get_contents(ACCOUNT_TEMPLATE_DIR . "/responses/forgot_username_thankyou.html"));
			return true;
		}
	
}


function sendForgotUsername($to, $from, $username)
{
	$textMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_forgot_username.txt");
	$textMessage = str_replace("{{username}}", $username, $textMessage);
	$textMessage = str_replace("{{code}}", $verificationCode, $textMessage);
	// verificartion link
	//verification page link
	$htmlMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_forgot_username.html");
	$htmlMessage = str_replace("{{username}}", $username, $htmlMessage);
	$htmlMessage = str_replace("{{code}}", $verificationCode, $htmlMessage);
		
	$boundary = uniqid('np');
	$headers = 'From: ' . $from . "\r\n";	
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
	$headers .= "Content-Transfer-Encoding: 7bit\r\n";

	$message = "\n\n--" . $boundary . "\n";
	$message	.= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";
	$message .= $textMessage;
	$message .= "\n\n--" . $boundary . "\n";		
	$message	.= "Content-Type: text/html; charset=\"UTF-8\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";

	$message .= $htmlMessage;
	$message .= "\n\n";		
	mail($to, "Forgot Username Request", $message, $headers);
}


function failResponse($message)
{
?>
<div id="response">
	<div id="title">Failed</div>
	<div id="outcome" class="hidden">0</div>
   	<div id="reason" class="message"><?php echo($message);?></div>	
</div>
<?php
}

function successResponse($message)
{
?>	
<div id="response">
	<div id="title">Success</div>
	<div id="outcome" class="hidden">1</div>
   	<div id="message" class="message"><?php echo($message);?></div>	
</div>
<?php
}
?>