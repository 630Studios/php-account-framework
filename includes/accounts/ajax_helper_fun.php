<?php
	require_once('config.php');	//ajax_helper is called remotely so we dont have to worry about relative path to config.php
	require_once (SITE_ROOT . '/includes/SQL_CONNECTION.php');
	require_once (SITE_ROOT . '/includes/accounts/registration_fun.php');
	checkCommands();
	
	
function checkCommands()
{
	
	if (!isset($_POST["command"]))
		return;
		
	$command = $_POST["command"];
	
	switch($command){
		case "check_username":
			checkUsername();
			break;	
			
		case "check_email":
			checkEmail();
			break;
			
		case "register_user":

			checkForRegistration();
			break;	
		
	}
		
}

function checkUsername()
{
	if (isset($_POST["username"]))
	{
	
		$db = $GLOBALS["db"];
		$username = trim(mysqli_real_escape_string($db,$_POST["username"]));
		$results = $db->query("SELECT * FROM users WHERE username='" . $username . "'");
		$userCount = $results->num_rows;
		
		if ($userCount > 0)
		{
			echo 0;
		}else{
			echo 1;
		}
	}
}

function checkEmail()
{
	if (isset($_POST["email"]))
	{
	
		$db = $GLOBALS["db"];
		$email = trim(mysqli_real_escape_string($db,$_POST["email"]));
		$results = $db->query("SELECT * FROM users WHERE email='" . $email . "'");
		$userCount = $results->num_rows;
		
		if ($userCount > 0)
		{
			echo 0;
		}else{
			echo 1;
		}
	}
}
?>
