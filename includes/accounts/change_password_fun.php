<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function checkForChangePassword()
{
	if (!isLoggedIn())	//cant do this when your not logged in
	{
		return false;
	}
	if (!isset($_POST["btnChangePassword"])) //make sure the button from change_password_form was used
	{
		failResponse("Not submitted from change password form.");
		return false;
	}
		
	if (!isset($_POST["password"]))	//shouldnt happen, saftey check for people poking around
	{
		failResponse("No password field.");
		return false;
	}
		
	if (!isset($_POST["confirmPassword"])) //shouldnt happen, saftey check for people poking around
	{
		failResponse("No confirmPassword field.");
		return false;
	}
		

	$db = $GLOBALS["db"];	//get global $db variable
	
	$password = trim(mysqli_real_escape_string($db, $_POST["password"]));
	$confirmPassword = trim(mysqli_real_escape_string($db, $_POST["confirmPassword"]));
	
	if (strlen($password) < 5)
	{
		failResponse("Password must be atleast 5 characters long.");
		return false;
	}
	
	if ($password != $confirmPassword)
	{
		failResponse("Password and confirmation password do not match.");
		return false;	
	}
	$passwordHash = AccountHelper::generatePasswordHash($password);
	$userID = $_SESSION["userID"];
	$sqlStr = "UPDATE users SET passwordHash = '{$passwordHash}' WHERE id = {$userID}";
	$db->query($sqlStr);
	successResponse("Password changed");
}



function failResponse($message)
{
?>
<div id="response">
	<div id="title">Failed</div>
	<div id="outcome" class="hidden">0</div>
   	<div id="reason" class="message"><?php echo($message);?></div>	
</div>
<?php
}

function successResponse($message)
{
?>	
<div id="response">
	<div id="title">Success</div>
	<div id="outcome" class="hidden">1</div>
   	<div id="message" class="message"><?php echo($message);?></div>	
</div>
<?php
}
?>