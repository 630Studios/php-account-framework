var validName = false;
var validEmail = false;
var validPassword = false;

$(document).ready(function() {  


         
		
		if ($('#registration').length)
		{
			
			initRegistration();	
		}
		
		if ($('#login').length) //check to see if we are on the login page
		{
			initLoginPage(); 
		}


		
		 
  
  });  
  
function hideModal()
{
		$('#notificationModal').removeClass('modalVisible');
		$('#notificationModal').addClass('modalHidden');
}


function showModal(title, message, okAction)
{
	$('#notificationModal').removeClass('modalHidden');
	$('#notificationModal').addClass('modalVisible');
	$('#notificationTitle').html(title);
	$('#notificationMessage').html(message);
	$('#notificationAccept').click(okAction);
}

function redirectToPage(redirectPage)
{
	window.location.href = redirectPage;	
}
  
function initLoginPage()
{
	$.get("includes/accounts/templates/forms/registration_form.php", function( regForm ) {
		$('#registrationContainer').html(regForm);
		
		$('#registration').hide();
	//		$('#registration input[type="submit"]').prop('disabled', true);
		$('#registration #btnBack').click(function( eventObject ){
					 eventObject.preventDefault();
					 $('#registration').hide();
					 $('#login').show();
		})
			
	
		$('#login #btnRegister').click(function( eventObject ){
					 eventObject.preventDefault();
					 $('#login').hide();
					 $('#registration').show();
		});

		initRegistration();		
			
	});
}

function initRegistration()
{

		if ($('#registration #btnRegister').length)
		{

			$('#registration #btnRegister').on('click', function(eventObject){
				eventObject.preventDefault();

				var username = $('#registration #username').val();
				var email = $('#registration #email').val();
				var confirmEmail = $('#registration #confirmEmail').val();
				var password = $('#registration #password').val();
				var confirmPassword = $('#registration #confirmPassword').val();
				var command = "register_user";
				
				$.post("includes/accounts/ajax_helper_fun.php", { command: command, username: username, email: email, confirmEmail: confirmEmail, password: password, confirmPassword: confirmPassword },  
				function(result){  

					var outcome = $(result).find('#outcome').text();

					
					if (outcome == 1)
					{
						var redirect = $(result).find('#redirect').text();
						var message = $(result).find('#message').text();
						showModal("Registration Success",message, function(){redirectToPage()});
					}else{
						var reason = $(result).find('#reason').text();
						showModal("Registration Failure", reason, hideModal);


					}
				});  
				
			});
		}else{
			alert("wtf");	
		}
		checkAllValidation();
		
		$('#registration #confirmEmail').on('input', function(){
			compareEmail();
			checkAllValidation();
		});
		
		$('#registration #email').on('input', function(){
			if($('#registration #email').val().length > 4)
			{
				compareEmail();
			}
			checkAllValidation();
		});
		
		$('#registration #password').on('input', function(){
			comparePassword();
			checkAllValidation();
		});
		
  		$('#registration #confirmPassword').on('input', function(){
			comparePassword();
			checkAllValidation();
		});
        //when button is clicked  
        $('#registration #username').on('input', function(){ 
			var min_chars = 3; 
            //run the character number check  
            if($('#registration #username').val().length >= min_chars){  
                //else show the cheking_text and run the function to check  
                check_username_availability();
				checkAllValidation();
            }else{

			}
        }); 
}

function compareEmail()
{
	var email = $('#registration #email').val().toLowerCase();
	var cEmail = $('#registration #confirmEmail').val().toLowerCase();

	if (email != cEmail)
	{
		$('#emailReq').html("Email and Confirm Email fields do not match.");
		$('#emailReq').addClass('validationError');

		validEmail = false;
	}else{
		$('#emailReq').html("");		
		validEmail = true;
		check_email_availability();
	}
}
  
function comparePassword()
{
	var pw = $('#registration #password').val();
	var cpw = $('#registration #confirmPassword').val();

	if (pw != cpw)
	{
		$('#passwordReg').html("Password and Confirm Password fields do not match.");
		$('#passwordReg').addClass('validationError');

		validPassword = false;
	}else{
		$('#passwordReg').html("");
		validPassword = true;
	}
}

function checkAllValidation()
{/*
	if ((validName == true) && (validPassword == true) && (validEmail == true))
	{
		$('#registration #btnRegister"]').prop('disabled', false);
	}else{
		$('#registration #btnRegister').prop('disabled', true);
	}*/
}


  
//function to check username availability  
function check_username_availability(){  
  
        //get the username  
        var username = $('#registration #username').val();  
        //use ajax to run the check 
		var command = "check_username";
        $.post("includes/accounts/ajax_helper_fun.php", { command: command, username: username },  
            function(result){  
                //if the result is 1  
                if(result == 1){  
                    //show that the username is available  
                    $('#usernameReq').html(username + ' is Available'); 
					$('#usernameReq').removeClass();
					$('#usernameReq').addClass('validationSuccess');
					validName = true;
                }else{  
                    //show that the username is NOT available  
                    $('#usernameReq').html(username + ' is already in use.');
					$('#usernameReq').removeClass();
					$('#usernameReq').addClass('validationError');
					validName = false;
                }  
        });  
  
}

//function to check username availability  
function check_email_availability(){  
  
        //get the username  
        var email = $('#registration #email').val();  
		var command = "check_email";
        //use ajax to run the check  
        $.post("includes/accounts/ajax_helper_fun.php", { command: command, email: email },  
            function(result){  
                //if the result is 1  
                if(result == 1){  
                    //show that the username is available  
					validEmail = true;
                }else{  
                    //show that the username is NOT available  
                    $('#emailReq').html('Email address is already in use.');
					$('#emailReq').removeClass();
					$('#emailReq').addClass('validationError');
					validEmail = false;
                }  
        });  
  
}


