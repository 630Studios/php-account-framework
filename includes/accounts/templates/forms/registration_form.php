
<div class="accountForm magictime swashIn" id="registration">
    <div class="title">Registration</div>
    <form method="post" >
        <div class="">
            <div class="label">Username</div> <input pattern="[a-zA-Z0-9_]{5,}" name="username" id="username" type="text" required="required" />
            <div class="" id="usernameReq"></div>
            <div class="input-details">Minimum of 5 characters.<br /> Alphanumeric and underscores only.</div>
            <div class="label">Email</div> <input name="email" id="email" type="email" required="required" /><br />
            <div class="" id="emailReq"></div> 
            <div class="input-details">Must be a valid email.<br /> A verification email will be sent to this address.</div>
            <div class="label">Confirm</div> <input name="confirmEmail" id="confirmEmail" type="email" required="required" /><br />
            <div class="input-details">Must match email field.</div>
            <div class="label">Password</div> <input pattern=".{5,}" name="password" id="password" type="password" required="required" /><br />
            <div class="input-details">Minimum of 5 characters</div>
            <div class="label">Confirm</div> <input pattern=".{5,}" name="confirmPassword" id="confirmPassword" type="password" required="required" /><br />
            <div class="" id="passwordReg"></div>
            <div class="input-details">Password and confirm password must match.</div>
            <br />
            <div ><div class="left-button"><a href="login.php" id="btnBack" name="btnBack">Goto Login</a></div><input type="submit" id="btnRegister" name="btnRegister" value="Register" class="right-button"></div>
        </div>
    </form>
<div>
