<?php
session_start();

require_once("config.php");
require_once(SITE_ROOT . "/includes/SQL_CONNECTION.php"); 
require_once(ACCOUNTS_DIR . "/classes/AccountHelper.php");
require_once(ACCOUNTS_DIR . "/login_fun.php");
checkForLogin();


class UserData{
	public $id;
	public $username;
	public $email;	
	
	function __construct($id, $username, $email)
	{
		$this->id = $id;
		$this->username = $username;
		$this->email = $email;	
	}


}
?>