<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");

class AccountHelper{
	
	public static function generatePasswordHash($password)
	{
		$cost = 10;
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$salt = sprintf("$2a$%02d$", $cost) . $salt; // Prefixed with "$2a$" means were usingBlowfish algorithm. Next two digits are the cost parameter.
		$passwordHash = crypt($password, $salt); // Hash the password with the salt
		return $passwordHash;
	}
	
	public static function getCryptoKey($halfSize)
	{
		return bin2hex(openssl_random_pseudo_bytes($halfSize));	
	}

}

?>