<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function handleForgotPassword()
{
		
		if(!isset($_POST['username']))
			return false;
			

		$db = $GLOBALS["db"];

		
		$username = trim(mysqli_real_escape_string($db,$_POST["username"]));
		
		if (strlen($username) < 3)
		{
			failResponse("Username is not in valid format.");
			return false;	
		}
		
		$results = $db->query("SELECT * FROM users WHERE username='" . $username . "'");
		$userCount = $results->num_rows;
		
		if ($userCount <= 0) // if we have more then one record either the username or email address is already in use.
		{
			failResponse("No such username in use.");
			return false;
//			invalidLoginResponse("Username or Email address already in use.");
		}else{ // no matches, we can add the user
		
			$row = $results->fetch_assoc();
            $userID = $row["id"];
			$email = $row["email"];
			$loginToken = AccountHelper::getCryptoKey(64);
						
			$sqlStr = "DELETE FROM login_tokens WHERE userID = {$userID}";
			$db->query($sqlStr);
			$sqlStr = "INSERT INTO login_tokens (userID, token) VALUES ( {$userID}, '{$loginToken}')";
			$db->query($sqlStr);

			
			sendForgotPassword($email, ACCOUNT_EMAIL, $username, $loginToken);
			successResponse(file_get_contents(ACCOUNT_TEMPLATE_DIR . "/responses/forgot_password_thankyou.html"));
			return true;
		}
	
}


function sendForgotPassword($to, $from, $username, $token)
{
	$textLink = $textLink = 'http://' . $_SERVER['HTTP_HOST'] . '/' . LOGIN_PAGE . "?username={$username}&token={$token}";
	$htmlLink = $htmlLink = "<a href=\"{$textLink}\">Login</a>";
	$textMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_forgot_password.txt");
	$textMessage = str_replace("{{username}}", $username, $textMessage);
	$textMessage = str_replace("{{login-link}}", $textLink, $textMessage);
	// verificartion link
	//verification page link
	$htmlMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_forgot_password.html");
	$htmlMessage = str_replace("{{username}}", $username, $htmlMessage);
	$htmlMessage = str_replace("{{login-link}}", $htmlLink, $htmlMessage);
		
	$boundary = uniqid('np');
	$headers = 'From: ' . $from . "\r\n";	
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
	$headers .= "Content-Transfer-Encoding: 7bit\r\n";

	$message = "\n\n--" . $boundary . "\n";
	$message	.= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";
	$message .= $textMessage;
	$message .= "\n\n--" . $boundary . "\n";		
	$message	.= "Content-Type: text/html; charset=\"UTF-8\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";

	$message .= $htmlMessage;
	$message .= "\n\n";		
	mail($to, "Forgot Password Request", $message, $headers);
}


function failResponse($message)
{
?>
<div id="response">
	<div id="title">Failed</div>
	<div id="outcome" class="hidden">0</div>
   	<div id="reason" class="message"><?php echo($message);?></div>	
</div>
<?php
}

function successResponse($message)
{
?>	
<div id="response">
	<div id="title">Success</div>
	<div id="outcome" class="hidden">1</div>
   	<div id="message" class="message"><?php echo($message);?></div>	
</div>
<?php
}
?>