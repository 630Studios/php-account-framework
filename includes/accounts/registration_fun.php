<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function checkForRegistration()
{


		$db = $GLOBALS["db"];
		$username = trim(mysqli_real_escape_string($db,$_POST["username"]));
		$email = trim(mysqli_real_escape_string($db,$_POST["email"]));
		$confirmEmail = trim(mysqli_real_escape_string($db,$_POST["confirmEmail"]));
		$password = trim(mysqli_real_escape_string($db, $_POST["password"]));
		$confirmPassword = trim(mysqli_real_escape_string($db, $_POST["confirmPassword"]));
		
		if (strlen($username) < 3)
		{
			failResponse("Username must be atleast 3 characters long.");
			return false;	
		}
		
		if (strlen($email) < 5)
		{
			failResponse("Emails must be at least 5 characters long.");
			return false;	
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			failResponse("Email is not in valid format.");
			return false;
			//invalid email format
		}
		
		if ($email != $confirmEmail)
		{
			failResponse("Email and confirmation email do not match.");
			return false;	
		}
		
		if (strlen($password) < 5)
		{
			failResponse("Password must be atleast 5 characters long.");
			return false;
		}
	
		if ($password != $confirmPassword)
		{
			failResponse("Password and confirmation password do not match.");
			return false;	
		}
		
		$results = $db->query("SELECT * FROM users WHERE username='" . $username . "' OR email='" . $email . "'");
		$userCount = $results->num_rows;
		
		if ($userCount > 0) // if we have more then one record either the username or email address is already in use.
		{
			failResponse("Username or email already in use.");
			return false;
//			invalidLoginResponse("Username or Email address already in use.");
		}else{ // no matches, we can add the user
		 	  $passwordHash = AccountHelper::generatePasswordHash($password); 
			  $activation =  AccountHelper::getCryptoKey(64);
			  
			  $sqlStr = "INSERT INTO users (username, email, passwordHash, verificationCode) VALUES ('{$username}','{$email}','{$passwordHash}','{$activation}')";
			  $results = $db->query($sqlStr);
		  
//			  mail($email, "Account Verification - Action Required","Verification code: {$activation}");
			  sendRegistration($email, ACCOUNT_EMAIL, $username, $activation);
			  successResponse(file_get_contents(ACCOUNT_TEMPLATE_DIR . "/responses/reg_thankyou.html"), $GLOBALS["REGISTRATION_REDIRECT"]);
			  return true;
		}
	
}


function sendRegistration($to, $from, $username, $verificationCode)
{
	$textLink = 'http://' . $_SERVER['HTTP_HOST'] . "/verification.php?username={$username}&verification={$verificationCode}";
	$htmlLink = "<a href=\"{$textLink}\">Verify Account</a>";
	
	$textMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_registration.txt");
	$textMessage = str_replace("{{username}}", $username, $textMessage);
	$textMessage = str_replace("{{code}}", $verificationCode, $textMessage);
	$textMessage = str_replace("{{verification-link}}", $textLink, $textMessage);

	$htmlMessage = file_get_contents(ACCOUNT_TEMPLATE_DIR . "/emails/email_registration.html");
	$htmlMessage = str_replace("{{username}}", $username, $htmlMessage);
	$htmlMessage = str_replace("{{code}}", $verificationCode, $htmlMessage);
	$htmlMessage = str_replace("{{verification-link}}", $htmlLink, $htmlMessage);
		
	$boundary = uniqid('np');
	$headers = 'From: ' . $from . "\r\n";	
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
	$headers .= "Content-Transfer-Encoding: 7bit\r\n";

	$message = "\n\n--" . $boundary . "\n";
	$message	.= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";
	$message .= $textMessage;
	$message .= "\n\n--" . $boundary . "\n";		
	$message	.= "Content-Type: text/html; charset=\"UTF-8\"\n";
	$message	.= "Content-Transfer-Encoding: 7bit\n\n";
	$message .= $htmlMessage;
	$message .= "\n\n";		
	mail($to, "Account Registration", $message, $headers);
}


function failResponse($message)
{
?>
<div id="response">
	<div id="title">Registration Failed</div>
	<div id="outcome" class="hidden">0</div>
   	<div id="reason" class="message"><?php echo($message);?></div>	
</div>
<?php
}

function successResponse($message, $redirectPage)
{
?>	
<div id="response">
	<div id="title">Registration Success</div>
	<div id="outcome" class="hidden">1</div>
   	<div id="message" class="message"><?php echo($message);?></div>	
   	<div id="redirect" class="hidden"><?php echo($redirectPage);?></div>	
</div>
<?php
}
?>