<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/accounts/config.php");
require_once(SITE_ROOT . '/includes/SQL_CONNECTION.php');
function checkForLogin()
{
	
	if (isLoggedIn())	
		return;
		
	if (isset($_POST["btnLogin"]))
	{
		if (isset($_POST["username"]) && isset($_POST["password"]))
		{
			$db = $GLOBALS["db"];
			$username = mysqli_real_escape_string($db,$_POST["username"]);
			$password = mysqli_real_escape_string($db, $_POST["password"]);	
	
			validateLogin($username, $password);
		}
		return;
	}
	
	if (isset($_GET["token"]) && isset($_GET["username"]))
	{
		$db = $GLOBALS["db"];
		$username = mysqli_real_escape_string($db,$_GET["username"]);
		$token = mysqli_real_escape_string($db,$_GET["token"]);
		 validateLoginWToken($username, $token);
		 return;

	}
}

function validateLoginWToken($username, $token)
{
	$db = $GLOBALS["db"];		//set a local variable to the db object
	$sqlStr = "SELECT users.id AS id, users.email as email FROM users INNER JOIN login_tokens ON users.id=login_tokens.userID WHERE users.verificationCode='0' AND users.username='{$username}' AND login_tokens.token='{$token}';";
	$results = $db->query($sqlStr);
	$resultCount = $results->num_rows;
	
	
	if ($resultCount == 1) // did we find a user? and just 1 user?
	{
		$userData = $results->fetch_assoc();	//grab the users information
		$userID = $userData["id"];
		$email = $userData["email"];
				
		$_SESSION["userID"] = $userID;
		$_SESSION["username"] = $username;
		$_SESSION["email"] = $email;
				
		$ip = $_SERVER["REMOTE_ADDR"];
		$sqlStr = "INSERT INTO login_history (userID, ip) VALUES ({$userID}, '{$ip}')";
		$results = $db->query($sqlStr);
		
		$sqlStr = "DELETE FROM login_tokens WHERE userID={$userID} AND token='{$token}';";
		$results = $db->query($sqlStr);

		return true;
	}else{
		return false;
	}
	
}


function validateLogin($username, $password)
{
	$db = $GLOBALS["db"];		//set a local variable to the db object
	
	$results = $db->query("SELECT * FROM users WHERE username='" . $username . "'");	//check and see if we have a matching user
	$userCount = $results->num_rows;	//see how many users match, should be either 0 or 1 as there should be no duplicate usernames

	if ($userCount == 1)	//we found our users
	{
		$userData = $results->fetch_assoc();	//grab the users information
		$passwordHash = $userData["passwordHash"]; 	//grab the hash of the password so we can check against it
	 	/*lets make sure the password matches. hashing a hash against the password should return the same hash if valid password was supplied*/
		if ( hash_equals($passwordHash, crypt($password, $passwordHash)) ) {
			$verificationCode = $userData["verificationCode"];		//grab the users verificationCode so we can see if they verified their account
			
			if ($verificationCode == "0")		//the user has verified
			{
				$userID = $userData["id"];
				$email = $userData["email"];
				
				$_SESSION["userID"] = $userID;
				$_SESSION["username"] = $username;
				$_SESSION["email"] = $email;
				
			
				$ip = $_SERVER["REMOTE_ADDR"];
				$sqlStr = "INSERT INTO login_history (userID, ip) VALUES ({$userID}, '{$ip}')";
				$results = $db->query($sqlStr);
				
			}else{
				//echo("User has not verified");
			}
		 }else{
				//echo("Invalid password");
		 }
	}else{
				//echo("invalid username");
	}
	
}

function isLoggedIn()
{
	if (isset($_SESSION["userID"]))
	{
		return true;
	}else{
		return false;
	}
}

function hash_equals($str1, $str2) {
    if(strlen($str1) != strlen($str2)) {
      return false;
    } else {
      $res = $str1 ^ $str2;
      $ret = 0;
      for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
      return !$ret;
    }
  }

?>