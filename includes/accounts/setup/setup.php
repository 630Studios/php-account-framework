<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
body,html{
	padding:0px;
	margin:0px;
	height:100%;
	width:100%;	
}
body{
	background-color:#151515;
	color:#fff;

}
body > div{

	background-color:#333;
	font-family:"Lucida Console", Monaco, monospace;
	font-size:10px;
    width:1000px;
    margin-left:auto;
    margin-right:auto;
	
}


body > div > div{

	height:30px;

	
}

body > div div div{
	border:none;	
	display:inline-block;
}

body > div div > div:first-child{
	width:125px;
	background-color:#333;
	padding-left:20px;
	color:#fff;
		line-height:30px;
}

body > div div > div:nth-child(2){
padding-left:20px;	
}

body > div input[type=text], body > div input[type=email]{
	height:20px;
	padding-left:10px;
	width:200px;
	font-family:"Lucida Console", Monaco, monospace;
	font-size:10px;
	margin-right:20px;
	background-color:#222;
	border:1px solid #555;
	opacity:0.5;
	color:#fff;
}

body > div input[type=text]:focus, body > div input[type=email]:focus{
	opacity:1;

}

.header{
	text-align:center;
	font-size:14px;
	font-weight:bold;
	background-color:#444;
	margin-top:10px;
	margin-bottom:10px;
	color:#fff;
	text-shadow:2px 2px 3px #000;
	border:1px solid #555;
	height:30px;
	line-height:30px;
}

.description{
	width:350px;
	margin:10px auto;
	font-size:10px;
	line-height:14px;	
	color:#999

}

.buttonContainer > #btnInstall{
display:block;
margin:10px auto;
height:30px;
width:150px;
background-color:#333;
color:#fff;
border:1px solid #555;
}
.buttonContainer > #btnInstall:hover{
display:block;
margin:10px auto;
height:30px;
width:150px;
background-color:#222;
color:#fff;
border:1px solid #555;
}

.installItem{
height:20px;
padding-left:20px;
color:#6F0;
display:block;
font-size:12px;
line-height:14px;
font-family:"Courier New", Courier, monospace;
}
.installItem > pre{
display:inline;	
color:#fff;
}

.installError{
height:20px;
padding-left:20px;
color:#900;
display:block;
font-size:12px;
line-height:14px;
font-family:"Courier New", Courier, monospace;
}
.installError > pre{
display:inline;	
color:#FFF;
}

.installWarning{
height:20px;
padding-left:20px;
color:#FF0;
display:block;
font-size:12px;
line-height:14px;
font-family:"Courier New", Courier, monospace;
}
.installWarning > pre{
display:inline;	
color:#FFF;
}

.createdBy{
position:fixed;
left:100%;
top:100%;
width:350px;
margin-left:-350px;
height:100px;
margin-top:-100px;
display:block;
z-index:1;

background-color:#000;
color:#fff;
font-family: 'Alegreya Sans SC', sans-serif;
opacity:0.05;
	border-top-left-radius:15px;
}

.createdBy:hover{
	opacity:1;
	transition:ease-in 0.5s;
}



.createdBy > div{
	margin:10px;	
}

.createdBy  a{
	color:#F90;
	text-decoration:none;
}

.createdBy  a:hover{
	color:#F90;
	text-decoration:underline;
}

</style>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,900' rel='stylesheet' type='text/css'>
</head>

<body>

<?php

	if (isset($_POST["btnInstall"]))
	{
		installFramework();
	}else{
		?>
        
	<span class="createdBy">
    <div>Created By: Jason Penick</div>
    <div>Email: StrikeBackStudios@gmail.com</div>
    <div>Bitbucket: <a href="http://bitbucket.org/630Studios/" target="_blank">http://bitbucket.org/630Studios/</a></div>
    </span>
        
        <?php
		displayForms();
	}
?>


</body>
</html>



<?php

function displayForms()
{
?>
<div style="display: flex;flex-direction: column;width:600px;">
    <form action="setup.php" method="post" target="runScript">
        <div style="display: flex;flex-direction: column;">
            <div class="header">General Settings</div>
            <div class="description">General settings needed by the Account Framework.</div>
            <div><div>Site Name:</div><div> 	<input type="text" name="txtSiteName" id="txtSiteName" placeholder="Enter Site Name" required="required" /></div></div>
            <div><div>Account Email:</div><div><input type="email" name="txtAccountEmail" id="txtAccountEmail" placeholder="Enter Email" required="required" /></div></div>
        </div>
        <div style="display: flex;flex-direction: column;">
            <div class="header">Database Settings</div>
            <div class="description">Required information for connecting to your sites database. You must already have a database and user setup in order to install the Account Framework.</div>
            <div><div>DB Server:</div><div> 	<input type="text" name="txtDBServer" id="txtDBServer" placeholder="localhost" required="required" /></div></div>
            <div><div>DB Name:</div><div> 		<input type="text" name="txtDBName" id="txtDBName" placeholder="MyDatabase" required="required" /></div></div>
            <div><div>DB User:</div><div> 		<input type="text" name="txtDBUser" id="txtDBUser" placeholder="MyDatabaseUser" required="required" /></div></div>
            <div><div>DB Password:</div><div> 	<input type="text" name="txtDBPassword" id="txtDBPassword" placeholder="MyDatabasePassword" required="required" /></div></div>
        </div>
        <div style="display: flex;flex-direction: column;">
            <div class="header">Pages</div>
            <div class="description">These are pages used by the account system. The default values are perfectly fine unless you have renamed any of the pages in which case reflect those changes here.</div>
            <div><div>Login:</div><div> 		<input type="text" name="txtLoginPage" id="txtLoginPage" value="login.php" required="required" /></div></div>
            <div><div>Registration:</div><div>	<input type="text" name="txtRegistrationPage" id="txtRegistrationPage" value="registration.php" required="required" /></div></div>
            <div><div>Verification:</div><div>	<input type="text" name="txtVerificationPage" id="txtVerificationPage" value="verification.php" required="required" /></div></div>
            <div><div>Profile:</div><div>		<input type="text" name="txtProfilePage" id="txtProfilePage" value="profile.php" required="required" /></div></div>
            <div><div>Forgot Username:</div><div><input type="text" name="txtForgotUsernamePage" id="txtForgotUsernamePage" value="fogot_username.php" required="required" /></div></div>
            <div><div>Forgot Password:</div><div><input type="text" name="txtForgotPasswordPage" id="txtForgotPasswordPage" value="forgot_password.php" required="required" /></div></div>
            <div class="buttonContainer">		<input type="submit" id="btnInstall" name="btnInstall" value="Install"  /></div>
        </div>
    </form>
    <div>
        <div class="header">Output Window</div>
        <iframe src="" id="runScript"  name="runScript" style="width:600px;height:400px;margin:20px;border:1px solid #555;display:block;" />
    </div>
</div>    



<?php
}

function installFramework()
{
	$SITE_ROOT = $_SERVER["DOCUMENT_ROOT"] . "/";
	$INCLUDE_FOLDER = $_SERVER["DOCUMENT_ROOT"] . "/includes/";
	$PAGE_DIR = $INCLUDE_FOLDER . "accounts/setup/pages/";
   
	
	$siteName = $_POST["txtSiteName"];
	$accountEmail = $_POST["txtAccountEmail"];
	$DBServer = $_POST["txtDBServer"];
	$DBName = $_POST["txtDBName"];
	$DBUser = $_POST["txtDBUser"];
	$DBPassword = $_POST["txtDBPassword"];
	$loginPage = $_POST["txtLoginPage"];
	$registrationPage = $_POST["txtRegistrationPage"];
	$verificationPage = $_POST["txtVerificationPage"];
	$profilePage = $_POST["txtProfilePage"];
	$forgotUsernamePage = $_POST["txtForgotUsernamePage"];
	$forgotPasswordPage = $_POST["txtForgotPasswordPage"];
	
	 $db = new mysqli($DBServer, $DBUser, $DBPassword, $DBName);
	 
	echo "<br />";
	echo "<span class=\"installItem\"><pre>[MySQL]  </pre>Testing Connection.</pre></span>";
	/*
	 * Create a connection to the DB with the user supplied information.
	 * If it fails let the user know why.
	 */
	$db = new mysqli($DBServer, $DBUser, $DBPassword, $DBName);
	if ($db->connect_errno > 0)
	{
		echo "<span class=\"installError\"><pre>[MySQL]Connection Failed.</pre></span><span class=\"installError\">";
		flush();
		die('Unable to connect to database [' . $db->connect_error . ']');
		return;
	}else{
		echo "<span class=\"installItem\"><pre>[MySQL]  </pre>Connection Success.</pre></span>";
	}
	
	/* Create the SQL_CONNECTION.php File */
	if (file_exists($INCLUDE_FOLDER . "SQL_CONNECTION.php"))
	{
		echo "<span class=\"installWarning\"><pre>[FILE]  </pre>SQL_CONNECTION.php already exists, skipping creation.</span>";
	}else{
		echo "<span class=\"installItem\"><pre>[FILE]  </pre>Creating SQL_CONNECTION.php....";
		flush();
		$sqlConnectionFile = file_get_contents("files/SQL_CONNECTION.txt");
		$sqlConnectionFile = str_replace("{{DBServer}}", $DBServer, $sqlConnectionFile);
		$sqlConnectionFile = str_replace("{{DBUser}}", $DBUser, $sqlConnectionFile);
		$sqlConnectionFile = str_replace("{{DBPassword}}", $DBPassword, $sqlConnectionFile);
		$sqlConnectionFile = str_replace("{{DBName}}", $DBName, $sqlConnectionFile);
		file_put_contents($INCLUDE_FOLDER . "SQL_CONNECTION.php", $sqlConnectionFile);
		echo "</pre>Success!</span>";
	}
	
	if (file_exists($INCLUDE_FOLDER . "accounts/config.php"))
	{
		echo "<span class=\"installWarning\"><pre>[FILE]  </pre>config.php already exists, skipping creation.</span>";
	}else{
		echo "<span class=\"installItem\"><pre>[FILE]  </pre>Creating config.php....";
		$configFile = file_get_contents("files/config.txt");
		$configFile = str_replace("{{SITE_NAME}}", $DBServer, $configFile);
		$configFile = str_replace("{{SITE_EMAIL}}", $DBUser, $configFile);
		$configFile = str_replace("{{LOGIN_PAGE}}", $DBPassword, $configFile);
		$configFile = str_replace("{{REGISTRATION_PAGE}}", $registrationPage, $configFile);
		$configFile = str_replace("{{VERIFICATION_PAGE}}", $verificationPage, $configFile);
		$configFile = str_replace("{{FORGOT_USERNAME_PAGE}}", $forgotUsernamePage, $configFile);
		$configFile = str_replace("{{FORGOT_PASSWORD_PAGE}}", $forgotPasswordPage, $configFile);
		$configFile = str_replace("{{PROFILE_PAGE}}", $profilePage, $configFile); 
		file_put_contents($INCLUDE_FOLDER . "accounts/config.php", $configFile);
		echo "</pre>Success!</span>";
	}
	echo "<span class=\"installItem\"><pre>[MySQL] </pre>Creating 'users' Table</pre></span>";
	flush();
	createUserTable($db);
	echo "<span class=\"installItem\"><pre>[MySQL] </pre>Creating 'login_history' Table</pre></span>";
	flush();
	createLoginHistoryTable($db);
	echo "<span class=\"installItem\"><pre>[MySQL] </pre>Creating 'login_tokens' Table</pre></span>";
	flush();
	createLoginTokenTable($db);
	echo "<span class=\"installItem\"><pre>[MySQL] </pre>Creating SQL maintenance Events</pre></span>";
	flush();
	createEvents($db);
	
	copyPage($SITE_ROOT, $PAGE_DIR, "index.txt", "index.php", true);
	copyPage($SITE_ROOT, $PAGE_DIR, "logout.txt", "login.php", true);
	copyPage($SITE_ROOT, $PAGE_DIR, "login.txt", $loginPage);
	copyPage($SITE_ROOT, $PAGE_DIR, "registration.txt", $registrationPage);
	copyPage($SITE_ROOT, $PAGE_DIR, "verification.txt", $verificationPage);
	copyPage($SITE_ROOT, $PAGE_DIR, "forgot_username.txt", $forgotUsernamePage);
	copyPage($SITE_ROOT, $PAGE_DIR, "forgot_password.txt", $forgotPasswordPage);
	copyPage($SITE_ROOT, $PAGE_DIR, "profile.txt", $profilePage);
	

	
}

function copyPage($SITE_ROOT, $PAGE_DIR, $srcPage, $destPage, $force = false)
{
   	echo "<span class=\"installItem\"><pre>[FILE]  </pre>Copying {$srcPage}</pre></span>";
	if (file_exists($SITE_ROOT . $destPage) && (force == false))
	{
		echo "<span class=\"installWarning\"><pre>[FILE]  </pre>{$srcPage} already exists, aborting copy.</span>";
	}else{
		file_put_contents($SITE_ROOT . $destPage, file_get_contents($PAGE_DIR . $srcPage));
	}	
}


function createUserTable($db)
{
	$createUserTable = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwordHash` varchar(255) NOT NULL,
  `verificationCode` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	$db->query($createUserTable);	
}

function createLoginTokenTable($db)
{
	$createLoginTokenTable = "CREATE TABLE IF NOT EXISTS `login_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($createLoginTokenTable);
}


function createLoginHistoryTable($db)
{
$createLoginHistoryTable = "CREATE TABLE IF NOT EXISTS `login_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($createLoginHistoryTable);
}


function createEvents($db)
{
	$createEvents = "CREATE DEFINER=`" . DB_USER . "`@`localhost` EVENT `clearExpiredLoginTokens` ON SCHEDULE EVERY 15 MINUTE STARTS '2016-02-19 17:07:23' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'clear event tokens older then 15 minutes' DO DELETE FROM login_tokens WHERE TIMESTAMPDIFF(MINUTE, createdAt, NOW()) > 15$$";
	//$db->query($createLoginTokenTable);
}

?>