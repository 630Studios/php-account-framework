<?php
require_once("includes/accounts/account_handler.php");
require_once("includes/accounts/forgot_password_fun.php");
/*check if we are logged in already, if so it makes no sense to be on this page*/
if (isLoggedIn())
{
	header('Location: index.php');
	die();
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Account Framework: Forgot Password</title>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,900' rel='stylesheet' type='text/css'>
<link href="css/base.css" rel="stylesheet" type="text/css" />
<link href="includes/accounts/css/login.css" rel="stylesheet" type="text/css" />
</head>

<body >
<script src="includes/jquery-2.2.0.js"></script>

	<?php 
	include "layout/header.php";

	$didSend = false;

	if (isset($_POST["username"]))
	{
		$didSend = handleForgotPassword();
	}
	
	if ($didSend != true)
	{
		include(ACCOUNT_TEMPLATE_DIR . "/forms/forgot_password_form.php");
	}


	closeDB(); //Always call closeDB() in at the bottom of any root page that includes account_handler.php or SQL_CONNECTION.php
	?>

</body>
</html>

