# PHP Account Framework#

This is a drop in account system for php.  
It handles login/registration/verification of user accounts for your website.  
  
It includes basic pages for login, registration and verification. Each page makes use of html5, css3 and jQuery with proper use fall-backs where features are not available.  
  
Additionally the login page is setup to make extensive use of html5 css3 and jQuery when available in order to provide a more seamless and interactive login, registration, and verification process on a single page.  

# How do I get set up? #

**1 )Download PHP Account Framework to your website**  
Grab a zip of the latest copy of the repository and extract it to your websites root directory.  You can also clone the repository to the sites web root directory.  

**2) Setup PHP Account Framework**  
Once the files are installed to your website you can navigate to Navigate to http://yoursite.com/index.php and a link is provided there to take you to the setup page.


### Contribution guidelines ###

This is my first public repo where it is possible people may wish to contribute to in some way. Im not sure how all that works so once I have it figured out I will update this section.